
let drawCircle = function(objectCanvas, x, y, procNumber) {
    let image = new Image();
    image.src = 'circle_1.png';
    image.width = 50;
    image.height = 50;

    image.onload = function() {
        objectCanvas.drawImage(image, x, y, 50, 70);
        objectCanvas.font = "20px Arial";
        objectCanvas.strokeText(procNumber, x + 19, y);
    };

};

Array.prototype.chunk = function (pieces) {
    pieces = pieces || 2;
    let len = this.length;
    let mid = (len/pieces);
    let chunks = [];
    let start = 0;
    for(let i = 0; i  <pieces; i++) {
        let last = start + mid;
        if (!len % pieces >= i) {
            last = last - 1
        }
        chunks.push(this.slice(start, last + 1) || []);
        start = last + 1;
    }
    return chunks;
};

let drawLine = function(objectCanvas, beginX, beginY, endX, endY) {
    objectCanvas.beginPath();
    objectCanvas.moveTo(beginX, beginY);
    objectCanvas.lineTo(endX, endY);
    objectCanvas.stroke();
};


let generateAnimation = function(count, objectCanvas) {
    let dx = 200;
    let i = 0;
    let step1 = [];

    let descProcesses = setInterval(function(){
        drawCircle(objectCanvas, dx, 150, i);

        i++;
        if (count === i) {
            let j = 0;
            let ddx = 200;
            clearInterval(descProcesses);
            objectCanvas.font = "30px Arial";
            objectCanvas.strokeText("Шаг 1", 100, 350);

            let doneStep1 = step1.chunk(count / 2);

            let descStep1 = setInterval(function () {
                //drawCircle(objectCanvas, ddx, 300, j);

                drawLine(objectCanvas, step1.chunk(count / 2)[j][1]['x'], step1.chunk(count / 2)[j][2]['x'],
                    step1.chunk(count / 2)[j][1]['y'], step1.chunk(count / 2)[j][2]['y']);
                console.log(step1.chunk(count / 2));

                j++;
                console.log(j);
                if (j == count / 2) {
                    clearInterval(descStep1);
                }

                ddx += 100;

            }, 300);
        }

        step1[i] = {x: dx, y: 150};

        dx += 100;
        //console.log(descProcesses);
    }, 300);
};

let changeAlgorithm = function () {
    $("#list").change(function () {
       switch ($("#list").val()) {
           case '1':
               $("#small_info").html('' +
                   'На первом шаге процессы, находящиеся на расстояние ' +
                   '1 друг от друг, обмениваются данными. ' +
                   'На втором шаге процессы, находящиеся на ' +
                   'расстояние 2 друг от друга, обмениваются ' +
                   'своими данными, а также данными, полученными ' +
                   'на предыдущем шаге. На третьем шаге процессы, ' +
                   'находящиеся на расстоянии 4 друг от друга обмениваются' +
                   ' своими данными, а также данными полученными на предыдущих' +
                   ' шагах. Таким образом, для числа процессов 2^n, ' +
                   'все процессы получают все данные с шагом равным lg p');
               console.log('1');
               break;
           case '2':
               $("#small_info").html('2');
               console.log('2');
               break;
           case '3':
               $("#small_info").html('3');
               console.log('3');
               break;
       }
    });
};

let recursiveDoubling = function (proccessCount, objectCanvas) {
    generateAnimation(proccessCount, objectCanvas);
};

let show = function () {
    $("#change").click(function() {
        console.log($("#small_info").html());
        if (!$("#small_info").is(':empty')) {
            $("#ctrl").css("display", "block");
        }
    });
};

let startAlgorithm = function(idAlgorithm, objectCanvas) {
    switch (idAlgorithm) {
        case '1':
            recursiveDoubling(parseInt($("#proc").val()), objectCanvas);
            break;
    }
};


$(document).ready(function() {
    let canvas = document.getElementById('animation');
    let objectCanvas = canvas.getContext('2d');

    canvas.width = 1280;
    canvas.height = 1024;

    changeAlgorithm();
    show();
    $("#start").click(function() {
        startAlgorithm('1', objectCanvas);
    })
});